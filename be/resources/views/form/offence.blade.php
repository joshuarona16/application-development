@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.offence_form')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Add Student Offence Information/Record</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">form</a></li>
                                <li class="breadcrumb-item"><a href="#!">Counciling</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h2 class="sub-title">Add Infomation and Record</h2>
                                        
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form role="form" id="validate" name="form" action="{{ route('form/offence/save') }}" method="POST">
                                            @csrf
                                            <div class="form-group">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Student ID</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="studid" name="studid" placeholder="Please Enter Your ID Number"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Full Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Lastname, Firstname M.I."/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="sex" name="sex">
                                                        <option selected disabled>Please select Gender</option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="age" name="age" placeholder="Please Enter age"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="course">
                                                    <option selected disabled>Please Select Course</option>
                                                        <option value="" disabled selected hidden>Please Select Course</option>
                                                        <option value="BSED">BSED</option>
                                                        <option value="BEED">BEED</option>
                                                        <option value="BPED">BPED</option>
                                                        <option value="BTLED">BTLED</option>
                                                        <option value="BPOS">BPOS</option>
                                                        <option value="BEED">BEED</option>
                                                        <option value="BACOMM">BACOMM</option>
                                                        <option value="BLISS">BLISS</option>
                                                        <option value="BSIT">BSIT</option>
                                                        <option value="BAEL">BAEL</option>
                                                        <option value="BSBIO">BSBIO</option>
                                                        <option value="BS ENTREPRENUERSHIP">BS ENTREPRENUERSHIP</option>
                                                        <option value="BSHM">BSHM</option>
                                                        <option value="BSTM">BSTM</option>
                                                        <option value="BSHRM">BSHRM</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Section</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="section" name="section" placeholder="Please Enter Section"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">School Year</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="schoolYear">
                                                        <option selected disabled>Please Select Year</option>
                                                        <option value="" disabled selected hidden>Please Select Year</option>
                                                        <option value="First Year">First Year</option>
                                                        <option value="Second Year">Second Year</option>
                                                        <option value="Third Year">Third Year</option>
                                                        <option value="Fourth Year">Fourth Year</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control" id="dates" name="dates" placeholder="Please select Date"/>
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Offence</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="offence" name="offence" placeholder="Please Enter Offence"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Offence Number</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="offence_num" name="offence_num">
                                                        <option selected disabled>Please select Offence Number</option>
                                                        <option value="1st Offence">1st Offence</option>
                                                        <option value="2nd Offence">2nd Offence</option>
                                                        <option value="3rd Offence">3rd Offence</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date of Offence</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control" id="date_offence" name="date_offence" placeholder="Please select Date of Offence"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Schedule of Offence</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control" id="schedule_offence" name="schedule_offence" placeholder="Please select Schedule of Offence"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Starting Time</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='time')" onblur="(this.type='text')" class="form-control" id="starting_time" name="starting_time" placeholder="Please select Starting of Offence"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">End Time</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='time')" onblur="(this.type='text')" class="form-control" id="end_time" name="end_time" placeholder="Please select End of Offence"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Venue</label>
                                                <div class="col-sm-10">
                                                <input type="text" class="form-control" id="venue" name="venue" placeholder="Please Enter Venue (Only inside of the University)"/>
                                                </div>
                                            </div>
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-sm btn-primary"><i class="icofont icofont-check-circled"></i>Save</button>
                                                    <button type= "reset"class="btn btn-sm btn-danger"><i class="icofont icofont-warning-alt"></i>Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $("form#validate").validate({
        rules: {
            studid: {
                    required: false,
                },
            fullName: {
                    required: false,
                },
            sex:{
                required: true,
            },
            age:{
                required: true,
            },
            course:{
                required: true,
            }, 
            schoolYear:{
                required: true,
            },    
            dates:{
                required: true,
            }, 
            offence:{
                required: true,
            },
            offence_num:{
                required: false,
            },
            date_offence:{
                required: false,
            }, 
            schedule_offence:{
                required: false,
            },  
            starting_time:{
                required: false,
            }, 
            end_time:{
                required: false,
            },
            venue:{
                required: false,
            },  
        },
        messages: {
            studid: {
                    required: "Please Enter ID Number ",
                },
            fullName: {
                    required: "Please Your Full Name",
                },
            sex:{
                required: "Please Select Gender",
            },
            age:{
                required: "Please Enter Age",
            },
            course:{
                required: "Please Select Course",
            },
            schoolYear:{
                required: "Please Select Year",
            },
            dates:{
                required: "Please Select Date",
            }, 
            offence:{
                required: "Please Enter Offence",
            }, 
            offence_num:{
                required: "Please Select Offence Number",
            },   
            date_offence:{
                required: "Please Select Date Offence",
            }, 
            schedule_offence:{
                required:"Please Enter Schedule",
            },
            starting_time:{
                required:"Please Enter Starting of Offence",
            }, 
            end_time:{
                required:"Please Enter End of Offence",
            },
            venue:{
                required:"Please Enter Venue (Only inside the University)",
            },        
        },
        
    });
    </script>
@endsection
