@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.guidance_counciling_report')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">View Student Guidance Counciling Information</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Edit</a></li>
                                <li class="breadcrumb-item"><a href="#!">Counciling</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Student Guidance Counciling Infomation View Detail</h4>  
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form id="validate" name="form" action="{{ route('form/guidance/save') }}" method="POST">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Student ID</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="teacherid" name="teacherid" value="{{ $value->stud_id }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Full Name</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="fullName" name="fullName" value="{{ $value->full_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="sex" name="sex" value="{{ $value->sex }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="age" name="age" value=" {{ $value->age }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="email" class="form-control text-danger" id="email" name="email" value="{{ $value->email }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Phone number</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="tel" class="form-control" id="phoneNumber" name="phoneNumber" value="{{ $value->phone_number }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course & Section</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="course" name="course" value="{{ $value->course }} - {{ $value->section }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">School Year</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="school_year" name="school_year" value="{{ $value->school_year }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="dates" name="dates" value="{{ $value->dates }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date and Schedule</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="doc" name="doc" value="{{ $value->date_of_council }} {{ $value->schedule_of_council }} AM/PM"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Personal Problem</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="problem" name="problem" value="{{ $value->problem }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Comment</label>
                                                <div class="col-sm-10">
                                                    <textarea readonly type="text" rows="5" cols="50" texarea class="form-control" id="comment" name="comment" value=""/>{{ $value->comments }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button readonly type="button" class="btn btn-sm btn-primary"><a href="{{ route('report/guidance/view') }}"><i class="icofont icofont-check-circled"></i>Back</a></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection