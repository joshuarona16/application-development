@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.guidance_counciling_report')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Edit Student Guidance Counciling Information</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Edit</a></li>
                                <li class="breadcrumb-item"><a href="#!">Counciling</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Student Guidance Counciling Infomation Edit Detail</h4>
                                        
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form id="validate" name="form" action="{{ route('form/guidance/update') }}" method="POST">
                                            @csrf
                                            <input type="text" hidden class="form-control" id="idUpdate" name="idUpdate" value="{{ $value->id }}">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">ID Number</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="studid" name="studid" value="{{ $value->stud_id }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Full Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullName" name="fullName" value="{{ $value->full_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="sex" name="sex">
                                                        <option value="male"{{ $value->sex =="male" ? 'selected' : '' }}>Male</option>
                                                        <option value="female" {{ $value->sex =="female" ? 'selected' : '' }}>Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="age" name="age" value=" {{ $value->age }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input type="email" class="form-control text-danger" id="email" name="email" value="{{ $value->email }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Phone number</label>
                                                <div class="col-sm-10">
                                                    <input type="tel" class="form-control" id="phoneNumber" name="phoneNumber" value="{{ $value->phone_number }}">
                                                </div>
                                            </div>                      
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="course">
                                                        <option value="BSED"    {{ $value->course =="BSED" ? 'selected' : '' }}>BSED</option>
                                                        <option value="BEED"    {{ $value->course =="BEED" ? 'selected' : '' }}>BEED</option>
                                                        <option value="BPED"    {{ $value->course =="BPED" ? 'selected' : '' }}>BPED</option>
                                                        <option value="BTLED"   {{ $value->course =="BTLED" ? 'selected' : '' }}>BTLED</option>
                                                        <option value="BPOS"    {{ $value->course =="BPOS" ? 'selected' : '' }}>BPOS</option>
                                                        <option value="BEED"    {{ $value->course =="BEED" ? 'selected' : '' }}>BEED</option>
                                                        <option value="BACOMM"  {{ $value->course =="BACOMM" ? 'selected' : '' }}>BACOMM</option>
                                                        <option value="BLISS"   {{ $value->course =="BLISS" ? 'selected' : '' }}>BLISS</option>
                                                        <option value="BSIT"    {{ $value->course =="BSIT" ? 'selected' : '' }}>BSIT</option>
                                                        <option value="BAEL"    {{ $value->course =="BAEL" ? 'selected' : '' }}>BAEL</option>
                                                        <option value="BSBIO"   {{ $value->course =="BSBIO" ? 'selected' : '' }}>BSBIO</option>
                                                        <option value="BS ENTREPRENUERSHIP" {{ $value->course =="BS ENTREPRENUERSHIP" ? 'selected' : '' }}>BS ENTREPRENUERSHIP</option>
                                                        <option value="BSHM"    {{ $value->course =="BSHM" ? 'selected' : '' }}>BSHM</option>
                                                        <option value="BSTM"    {{ $value->course =="BSTM" ? 'selected' : '' }}>BSTM</option>
                                                        <option value="BSHRM"   {{ $value->course =="BSHRM" ? 'selected' : '' }}>BSHRM</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Section</label>
                                                <div class="col-sm-10">
                                                    <input type="tel" class="form-control" id="section" name="section" value="{{ $value->section }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">School year</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="schoolYear">
                                                        <option value="First Year" {{ $value->school_year =="First Year" ? 'selected' : '' }}>First Year</option>
                                                        <option value="Second Year" {{ $value->school_year =="Second Year" ? 'selected' : '' }}>Second Year</option>
                                                        <option value="Third Year" {{ $value->school_year =="Third Year" ? 'selected' : '' }}>Third Year</option>
                                                        <option value="Fourth Year" {{ $value->school_year =="Fourth Year" ? 'selected' : '' }}>Fourth Year</option>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="tel" class="form-control" id="date" name="date" value="{{ $value->dates }}">
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date of Counciling</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="tel" class="form-control" id="date_counciling" name="date_counciling" value="{{ $value->date_of_council }}">
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Schedule</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="tel" class="form-control" id="schedule_counciling" name="schedule_counciling" value="{{ $value->schedule_of_council }}">
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Problem</label>
                                                <div class="col-sm-10">
                                                <select class="form-control" name="problem">
                                                        <option value="Anger Management" {{ $value->problem =="Anger Management" ? 'selected' : '' }}>Anger Management</option>
                                                        <option value="Bullying" {{ $value->problem =="Bullying" ? 'selected' : '' }}>Bullying</option>
                                                        <option value="Negative Attitude" {{ $value->problem =="Negative Attitude" ? 'selected' : '' }}>Negative Attitude</option>
                                                        <option value="Personal Hygiene" {{ $value->problem =="Personal Hygiene" ? 'selected' : '' }}>Personal Hygiene</option>
                                                        <option value="Adjustment" {{ $value->problem =="Adjustment" ? 'selected' : '' }}>Adjustment</option>
                                                        <option value="Honesty" {{ $value->problem =="Honesty" ? 'selected' : '' }}>Honesty</option>
                                                        <option value="Health"  {{ $value->problem =="Health" ? 'selected' : '' }}>Health</option>
                                                        <option value="Anxiety"  {{ $value->problem =="Anxiety" ? 'selected' : '' }}>Anxiety</option>
                                                        <option value="Theif/Vandalism"  {{ $value->problem =="Theif/Vandalism" ? 'selected' : '' }}>Theif/Vandalism</option>
                                                        <option value="Uncooperative/Defiant"  {{ $value->problem =="Uncooperative/Defiant" ? 'selected' : '' }}>Uncooperative/Defiant</option>
                                                        <option value="Others">Others</option>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Comment</label>
                                                <div class="col-sm-10">
                                                    <textarea type="text" rows="5" cols="50" texarea class="form-control" id="comment" name="comment" value="">{{ $value->comments }}</textarea>
                                                </div>
                                            </div> 
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-sm btn-success"><i class="icofont icofont-check-circled"></i>Update</button>
                                                    <button type="button" class="btn btn-sm btn-primary"><a href="{{ route('report/guidance/view') }}"><i class="icofont icofont-check-circled"></i>Back</a></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>  
window.onload = function() {  

  // ---------------
  // basic usage
  // ---------------
  var $ = new City();
  $.showProvinces("#province");
  $.showCities("#city");

  // ------------------
  // additional methods 
  // -------------------

  // will return all provinces 
  console.log($.getProvinces());
  
  // will return all cities 
  console.log($.getAllCities());
  
  // will return all cities under specific province (e.g Tacloban)
  console.log($.getCities("Tacloban")); 
  
}
</script>
@endsection