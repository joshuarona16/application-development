@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.guidance_counciling_report')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Student Guidance Counciling Information</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                    <li class="breadcrumb-item">
                                        <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Record & Data</a></li>
                                    <li class="breadcrumb-item"><a href="#!">Counciling</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
        
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Student Guidance Counciling Infomation & Record</h4>
                                        <div class="table-responsive">
                                            <table class="table align-items-center table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                                                <thead  class="thead-light" align="center">
                                                        <tr>
                                                            <th>ID Number</th>
                                                            <th>Name</th>
                                                            <th>Gender</th>
                                                            <th>Age</th>
                                                            <th>Email Address</th>
                                                            <th>Phone Number</th>
                                                            <th>Course & Section</th>
                                                            <th>Year</th>
                                                            <th>Date & Schedule of Counciling</th>
                                                            <th>Problems or Reason</th>
                                                            <th>Action</th>
                                                        </tr>
                                                </thead>
                                                    <tbody align="center">
                                                        @foreach ($tbl_guidance as $item)
                                                        <tr>
                                                            <td>{{ $item->stud_id }}</td>
                                                            <td>{{ $item->full_name }}</td>
                                                            <td>{{ $item->sex }}</td>
                                                            <td>{{ $item->age }}</td>
                                                            <td>{{ $item->email }}</td>
                                                            <td>{{ $item->phone_number }}</td>
                                                            <td>{{ $item->course }} - {{ $item->section }}</td>
                                                            <td>{{ $item->school_year }}</td>
                                                            <td>{{ $item->date_of_council }} {{ $item->schedule_of_council }} AM/PM</td>
                                                            <td>{{ $item->problem }}</td>
                                                            <td>
                                                                <a type ="button" class="label label-primary" style="font-size:15px;" href="{{ route('report/guidance/view/detail',['id'=>$item->id]) }}"><i class="fa fa-list-alt"></i></a>
                                                                <a type ="button" class="label label-success" style="font-size:15px;" href="{{ route('report/guidance/view/edit',['id'=>$item->id]) }}"><i class="fa fa-edit"></i></a>
                                                            
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                </tbody>                          
                                            </table>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
      $('#dataTable').DataTable(); // ID From dataTable 
      $('#dataTableHover').DataTable(); // ID From dataTable with Hover
    });
  </script>
   <script>
    $(document).ready(function () {
      $('#dataTables').DataTable(); // ID From dataTable 
      $('#dataTableHover').DataTable(); // ID From dataTable with Hover
    });
  </script
@endsection
