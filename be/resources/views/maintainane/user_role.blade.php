@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.user_role')

    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Administrator and User Registered Account</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Role</a></li>
                                <li class="breadcrumb-item"><a href="#!">Account</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
        
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <button type="button" data-toggle="modal" data-target="#addUser" class="btn btn-sm btn-primary" data-dismiss="modal"><i class="icofont icofont-eye-alt"></i>Save</button>
                                    <div class="card-block">
                                        <h4 class="sub-title">Registered Account Information and Record</h4>
                                        <div class="table-responsive">
                                            <table class="table align-items-center table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                                                <thead  class="thead-light" align="center">
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Role Name</th>
                                                        <th>Email</th>
                                                
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody align="center">
                                                @foreach ($useRole as $item)  
                                                    <tr>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $item->role_name }}</td>
                                                        <td>{{ $item->email }}</td>
                                                        <td>
                                                            <a type ="button" class="label label-primary" style="font-size:15px;" href="#" data-toggle="modal" data-target="#editUser"><i class="fa fa-edit"></i></a>
                                                            <a type ="button" class="label label-danger" style="font-size:15px;" href="#"><i class="fa fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>                          
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal large add-->        
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ti-close"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Name :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="" name="" placeholder="Enter name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="" name="" placeholder="Enter email">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Department :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="" name="" placeholder="Enter department">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Postion :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="" name="" placeholder="Enter postion">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Role Name :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="" name="" placeholder="Enter role name">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="icofont icofont-eye-alt"></i>Close</button>
                <button type="submit" class="btn btn-success" data-dismiss="modal"><i class="icofont icofont-eye-alt"></i>Save</button>
            </div>
        </div>
    </div>
</div><!--end Modal large-->

<!-- Modal large update-->        
<div class="modal fade" id="editUser" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ti-close"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Name :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="" name="" placeholder="Enter email">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Department :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="" name="" placeholder="Enter department">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Postion :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="" name="" placeholder="Enter postion">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Role Name :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="" name="" placeholder="Enter role name">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="icofont icofont-eye-alt"></i>Close</button>
                <button type="submit" class="btn btn-success" data-dismiss="modal"><i class="icofont icofont-eye-alt"></i>Update</button>
            </div>
        </div>
    </div>
</div><!--end Modal large-->

<script>
    $(document).ready(function () {
      $('#dataTable').DataTable(); // ID From dataTable 
      $('#dataTableHover').DataTable(); // ID From dataTable with Hover
    });
  </script>
   <script>
    $(document).ready(function () {
      $('#dataTables').DataTable(); // ID From dataTable 
      $('#dataTableHover').DataTable(); // ID From dataTable with Hover
    });
  </script>
@endsection
